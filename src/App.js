import React from 'react';
import './App.css';
import login from './page/login'
import { BrowserRouter, Switch, Route } from "react-router-dom";
import home from './page/home';
import about from './page/about';


function App() {

  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route exact path="/" component={login} />
          <Route exact path="/home" component={home} />
          <Route exact path="/about" component={about} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
