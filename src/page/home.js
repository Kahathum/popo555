import React, { Component } from 'react'
import { Link } from "react-router-dom";

class home extends Component {

    state = {
        names:""
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });
        // console.log(this.state.names)

    };

    handleClick = () => {
        window.alert("test"+this.state.names)
        console.log(this.state.names)
        this.props.history.push("/");
    }

    render() {
        // const {names} = this.state
        // this.state.names

        // names

        return (
            <div>
                <nav className="navbar" role="navigation" aria-label="main navigation">
                    <div className="navbar-brand">
                        <a className="navbar-item" href="https://bulma.io">
                            <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28" />
                        </a>

                        <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                    </div>

                    <div className="navbar-start">
                    
                        <a className="navbar-item">
                            Home </a>
                    <Link to ="/about">
                        <a className="navbar-item">
                            About </a>
                            </Link>
                        <a className="navbar-item">
                            Contact </a>
                   
                    </div>

                    <div className="navbar-end">
                        <div className="navbar-item">
                            <div className="buttons">
                                <a className="button is-primary">
                                    <strong>Sign up</strong>
                                </a>
                                <Link to="/">
                                    <a className="button is-light">Log in</a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </nav>

               
                <p className="label">Hello My World</p>
                <img style={{height:"20px"}} src="https://firebasestorage.googleapis.com/v0/b/popo555-ebab8.appspot.com/o/agile.png?alt=media&token=6f959c88-ecb2-4397-b3ba-adcfdd2506d1"/>
                <input type="text" id="names" onChange={this.handleChange}/>
                <button className="button is-info" onClick={this.handleClick}>Go</button>
            </div>



        )
    }
}

export default home
