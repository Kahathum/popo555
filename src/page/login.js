import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../css/login.css"

const a = 0;


class login extends Component {
    state = {
        id: "",
        pass: "",
        user: "fon"
    }


    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        });
    };

    render() {
       
        
        return (
            <div>
                <div>
                    <p className="label">Hello Bulma</p>
                    <img className="is-rounded" src="https://bulma.io/images/placeholders/128x128.png" />
                </div>

                <p className="label">Username</p>
                <input className="inputtext input is-info" type="text" id="id" placeholder="Username" onChange={this.handleChange} />
                {/* If else */}
                {this.state.id ? (<p className="help is-success">This username is available</p>) : null}
                

                <h1>{this.state.id}</h1>
                <br />
                <p className="label">password</p>
                <input className="inputtext input is-info" type="text" id="pass" placeholder="Password" onChange={this.handleChange} />
                <p className="help is-success">This password is available</p>
                <h1>{this.state.pass}</h1>
                <br />
                <Link to="/home">
                    <button className="button is-info">Submit</button>
                </Link>
            </div>
        )
    }
}
export default login